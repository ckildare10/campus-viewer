using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GetPano : MonoBehaviour
{
    [SerializeField] LongitudeLatitude[] placemarks;
    [SerializeField] Transform testPoint;
    Vector2 modifiers = new Vector2();

    void Start()
    {
        calculateModifiers();
        if (testPoint is not null)
        {
            // Vector2Int latLong = calculateLatLong(new Vector2(testPoint.position.x, testPoint.position.z));
            // Debug.Log(latLong.x);
            // Debug.Log(latLong.y);
            Debug.Log("Hello");

        }
    }

    void calculateModifiers()
    {
        float deltaX, deltaY, deltaLatitude, deltaLongitude;
        deltaX = placemarks[2].transform.position.x - placemarks[0].transform.position.x;
        deltaY = placemarks[1].transform.position.z - placemarks[0].transform.position.z;
        deltaLatitude = Int32.Parse(placemarks[2].getLatitude()) - Int32.Parse(placemarks[0].getLatitude());
        deltaLongitude = Int32.Parse(placemarks[1].getLongitude()) - Int32.Parse(placemarks[0].getLongitude());

        modifiers.x = deltaX / deltaLatitude;
        modifiers.y = deltaY / deltaLongitude;
    }

    Vector2Int calculateLatLong(Vector2 point)
    {
        Vector2Int latLong = new Vector2Int();
        latLong.x = (int) ((placemarks[0].transform.position.x - point.x) / modifiers.x + Int32.Parse(placemarks[0].getLatitude()));
        latLong.y = (int) ((placemarks[0].transform.position.z - point.y) / modifiers.y + Int32.Parse(placemarks[0].getLongitude()));
        return latLong;
    }
}
