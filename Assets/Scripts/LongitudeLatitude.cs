using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongitudeLatitude : MonoBehaviour
{
    [SerializeField] string longitude;
    [SerializeField] string latitude;

    public string getLongitude()
    {
        return longitude;
    }

    public string getLatitude()
    {
        return latitude;
    }
}
